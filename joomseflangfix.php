<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

class plgSystemJoomSEFLangFix extends JPlugin
{
    public function onAfterInitialise()
    {
        if (class_exists('SEFConfig') && SEFConfig::getConfig()->langMenuAssociations) {
            JLoader::register('JLanguageMultilang', dirname(__FILE__) . '/libraries/cms/language/multilang.php');
            JLoader::register('JLanguageAssociations', dirname(__FILE__) . '/libraries/cms/language/associations.php');
        }
    }
}